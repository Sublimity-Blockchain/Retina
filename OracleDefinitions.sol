pragma solidity ^0.4.25;
// The following are several AssessorOracle frameworks. Each smart contract is an
// entity to which people can make requests, and to which people can claim to
// have fulfilled the request by supplying evidence. This evidence is then assessed
// in any number of ways:
//    - Could be objectively assessed in the contract itself
//    - Could have the contract query an API to assess it
//    - Could require third parties to approve it
//    - Could require the original poster to approve it
//    - Could require submitter to create an augur market to assess correctness
//    - Could have the claimant submit info only a successful claimant could have,
//    such as the time of day the event should come to pass (prior to it passing)

// An abstract definition of an oracle that assesses whether a request was fulfilled
interface AssessmentOracle {

  // Tells whether this question has been satisfactorily completed
  function completed(uint questionID) public view returns(bool completed, address completer);

  // Mechanism for validating a response - either by running an operation on the
  // data, making a comparison, tallying votes, etc
  // TODO commenting out because i can't set an arbitrary data type here - is
  // there a way to do that? bytes perhaps?
  // function assess(uint questionID, uint/string/whatever data) internal returns (bool);

  // a way to submit arbitrary data for an available request
  function submit(bytes data) public returns(bool received, uint questionID);

}

// A way to put bounties on tasks given to an assessment oracle
contract AssessmentIncentiviser {

  AssessmentOracle public oracle;
  // a mapping from questionIDs to rewards, in wei
  mapping(uint=>uint) public bounties;

  // TODO how to make sure this is really a ao?
  constructor(AssessmentOracle _oracle) public payable {
    oracle = _oracle;
  }

  function settle(uint questionID) public {
    (bool completed, address completer) = oracle.completed(questionID);
    require(completed);
    // TODO must we only allow the completer to call this? could be convenient
    // for the funder to call it on behalf of the completer
    require(msg.sender == completer);
    completer.transfer(bounties[questionID]);
    bounties[questionID] = 0;
  }

  // For funding completion of the task
  function fund(uint questionID) public payable {
    bounties[questionID] += msg.value;
  }

  // revert random eth
  function () public payable {
    revert();
  }

}

contract AssessStringOracle is AssessmentOracle {



}

contract AssessImageOracle is AssessmentOracle {



}

// A framework for assessing tasks with integer evidence
contract AssessIntOracle is AssessmentOracle {

  function respond(uint questionID, uint claim) public returns (bool received);

}

// A silly example of one such market - guessing numbers for bounties
contract GuessMyNum is AssessIntOracle {

  struct Secret {
    uint num;
    bool guessed;
    address guesser;
  }
  Secret[] secrets;
  bool[] guessed;

  function submit(bytes data) public returns (bool received, uint id) {
    secrets.push(Secret(bytesToUint(data), false, msg.sender));
    return (true, secrets.length - 1);
  }

  function respond(uint questionID, uint claim) public returns (bool received) {
    require(!secrets[questionID].guessed);
    // this is the key part where we assess correctness. Maybe worth abstracting?? TODO
    secrets[questionID].guessed = assess(questionID, claim);
    if (secrets[questionID].guessed)
      secrets[questionID].guesser = msg.sender;
    return true;
  }

  function assess(uint questionID, uint claim) internal returns (bool correct) {
    return secrets[questionID].num == claim;
  }

  function completed(uint questionID) public view returns (bool completed, address completer) {
    return (secrets[questionID].guessed, secrets[questionID].guesser);
  }

  // public for debugging purposes
  function bytesToUint(bytes b) public pure returns (uint256){
    uint256 number;
    for(uint i=0;i<b.length;i++)
      number = number + uint(b[i])*(2**(8*(b.length-(i+1))));
    return number;
  }

}

